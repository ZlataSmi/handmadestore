export const PageTitle = ({children, className}) => {
    return (
        <div className={`page__title ${className ? className : ''}`}>
            <h1>{children}</h1>
        </div>
    )
}