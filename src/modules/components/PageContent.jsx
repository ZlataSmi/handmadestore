export const PageContent = ({children, className}) => {
    return (
        
        <div className={`page__content ${className ? className : ''}`}>
            {children}
        </div> 
    )
}