import { Carousel } from 'antd';
import { Link } from 'react-router-dom';
import './Home.css'

export const Home = () => {

    return (
        <>
        <Carousel className='home__carousel' autoplay dotPosition='right'>
            <div>
                <div className='home__carousel__element'
                style={{background: `linear-gradient(rgba(40, 40, 40, 0.7), rgba(140, 140, 140, 0.8)), url('/img/products/974824226fe4f1f00f3405b7bfa42291.jpg') center center / cover`}} >
                    <span className='carousel__span-new'>Новинка</span>
                    <h3 className='carousel__name'>ЖИРАФИК</h3>
                    <p className='carousel__description'>Очаровательный Жирафик в нежных розово-бежевых оттенках с зайкой в руках</p>
                    <span className='carousel__price'>100 BYN</span>
                    <Link to='/catalog/toys/899978-interernaya-kukla-zhirafik' 
                        className='carousel__goto-btn'>Перейти к товару →
                    </Link>
                </div>
            </div>
            <div> 
                <div className='home__carousel__element'
                style={{background: `linear-gradient(rgba(40, 40, 40, 0.7), rgba(140, 140, 140, 0.8)), url('/img/products/927ca1362934679e29238decdc1b3bff.jpg') center center / cover`}}>
                    <span className='carousel__span-new'>Акутальное</span>
                    <h3 className='carousel__name'>МУЖСКОЙ НАБОР МЫЛА</h3>
                    <p className='carousel__description'>В предверии праздников обратите внимание на замечательное мягкое глицериновое мыло</p>
                    <span className='carousel__price'>12 BYN</span>
                    <Link to="/catalog/soap/897108-konyak-limon-mylo-ruchnoy-raboty" 
                        className='carousel__goto-btn'>
                        Перейти к товару →
                    </Link>
                </div>
            </div>
            <div>
                <div className='home__carousel__element' 
                style={{background: `linear-gradient(rgba(40, 40, 40, 0.7), rgba(140, 140, 140, 0.8)), url('/img/products/9bddc4451cc71c415433b3fd0017da27.jpeg') center center / cover`}}>
                    <span className='carousel__span-new'>Снова в продаже</span>
                    <h3 className='carousel__name'>ЗОЛОТАЯ КОРОНА СОЛНЦЕ</h3>
                    <p className='carousel__description'>По многочисленным просьбам мы повторили эту замечательную корону</p>
                    <span className='carousel__price'>55 BYN</span>
                    <Link 
                        to='/catalog/jewelry/900305-zolotaya-korona-solntse-s-luchami'                     
                        className='carousel__goto-btn'>Перейти к товару →</Link>
                </div>
            </div>
            <div>
                <div className='home__carousel__element'
                style={{background: `linear-gradient(rgba(40, 40, 40, 0.7), rgba(140, 140, 140, 0.8)), url('/img/products/3431c03480526b2eab8e496b0407fee0.jpg') center center / cover`}}>
                    <span className='carousel__span-new'>Новинка</span>
                    <h3 className='carousel__name'>ДЕКОРАТИВНАЯ ПОДУШКА</h3>
                    <p className='carousel__description'> Яркая и мягкая подушка станет отличным дополнением в комнату ребёнка.</p>
                    <span className='carousel__price'>30 BYN</span>
                    <Link to="/catalog/decor/897521-dekorativnaya-tekstilnaya-myagkaya-podushka" className='carousel__goto-btn'>Перейти к товару →</Link>
                </div>
            </div>
        </Carousel>
        <div className='home__popular' >
            <Link to='/catalog/soap' 
                className='home__popular__element'
                style={{background: `url('/img/categories/e5605f244cf83184684b9466d343f2a3.png') center center / cover`}} 
            >
                <span className='home__popular__element__text'>Распродажа</span>
                <span className='home__popular__element__text'>В сезон отключения горячей воды спешите закупиться нашим ароматный мылом по выгодной цене</span>
            </Link>
            <Link to='/catalog/toys' 
                className='home__popular__element'
                style={{background: `url('/img/categories/8edd20c37bd8215c0aac927da123aebb.jpg') center center / cover`}}
            >
                <span className='home__popular__element__text'>Новая коллекция кукол</span>
                <span className='home__popular__element__text'>Интерьерные куклы и мягкие игрушки</span>

            </Link>
            <Link to='/catalog/decor/896194-pled-shokoladka' 
                className='home__popular__element'
                style={{background: `url('/img/products/b2692630f05cc941ff55b8991b583164.jpg') center center / cover`}}    
            >
                <span className='home__popular__element__text'>Лидер продаж</span>
                <span className='home__popular__element__text'>"Вкусный" шоколадный плед из натуральной шерсти</span>
            </Link>
            <Link to='/catalog/jewelry' 
                className='home__popular__element'
                style={{background: `url('/img/categories/4bdf3a167ed42814d873c3ccd16403de.jpg') center center / cover`}} 
            >
                <span className='home__popular__element__text'>Украшения к летнему сезону</span>
                <span className='home__popular__element__text'>Самые сочные и яркие!</span>
            </Link>
            <Link to='/catalog/souvenirs/898443-zolotaya-rybka-iz-konfet' 
                className='home__popular__element'
                style={{background: `url('/img/products/3e403a4e7283597f3dc1a634966a8492.jpeg') center center / cover`}} 
            >
                <span className='home__popular__element__text'>Лидер продаж</span>
                <span className='home__popular__element__text'>Золотая рыбка из конфет - хороший подарк на любой праздник</span>
            </Link>

        </div>
        </>
    )
}