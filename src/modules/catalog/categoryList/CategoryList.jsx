import { Skeleton } from "antd"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { PageContent, PageTitle } from "../../components"
import './CategoryList.css'

export const CategoryList = () => {

    const [categories, setCategories] = useState(undefined)
    const loadCategories = async () => {
        fetch(`https://api.jsonbin.io/v3/b/6485bca1b89b1e2299ad4366/`)
            .then(res=>res.json())
            .then(json => {
                console.log(json.record.categories)
                setCategories([...json.record.categories])
            }
        )
    }
 
    useEffect(() => {   
        loadCategories()
    }, [])
    return (
        <>
            <PageTitle>
                Каталог товаров ручной работы
            </PageTitle>
            {!categories ? 
                <div className="product__categories skeletons">
                <Skeleton active={true}/>
                <Skeleton active={true}/>
                <Skeleton active={true}/>
            </div>
             : 
                <PageContent className="product__categories">
                { 
                    categories.map((category, index) => 
                        <Link to={`${category.id}`} 
                            style={{background: `url(${category.background}) center center / cover`}} 
                            className="category"
                            key={index}
                        >
                            <div className="category__name">{category.name}</div>   
                        </Link>
                    )
                }
                </PageContent>
            }
            
        </>
    )
}