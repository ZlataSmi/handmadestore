import { Route, Routes } from "react-router-dom"
import { CategoryList } from "./categoryList"
import { ProductList } from "./productList"
import { ProductPage } from "./productPage"

export const Catalog = () => {
    return (
        (
            <Routes>
                <Route index element={<CategoryList/>}/>
                <Route path="/:categoryId" element={<ProductList/>} />
                <Route path="/:categoryId/:productId" element={<ProductPage/>}/>

            </Routes>
        )
    )
}