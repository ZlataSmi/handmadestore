import { observer } from "mobx-react-lite";
import { useState } from "react";
import { cartStore } from "../../../common/store/CartStore"

export const AddDeleteCartBtn = observer(({product, addedToCart}) => {
    
    const {cart, addToCart, deleteProduct} = cartStore;

    const isProductInCart = () => {
        if (cart.find((cartElement) => cartElement.id === product.id)) {
            return true
        } else 
            return false
    }  

    const [added, setAdded] = useState(addedToCart || isProductInCart()); 

    const handleClickAddDelete = () => {
        if(added === false) {
            setAdded(true);
            addToCart(product)
            
        } else {
            setAdded(false);
            deleteProduct(product.id)
            
        }
    }

    const handleClickAddDeleteCart = () => {
        deleteProduct(product.id)
    }

    return (
    <>
        <button 
            onClick={!addedToCart ? handleClickAddDelete : handleClickAddDeleteCart} 
            className={`product__cart__btn ${added === true ? 'delete__product' : 'add__product '}`}
        > 
            {!added ? 
                'Купить' 
                :
                'В корзине'
            }            
        </button>
    </>
    )
})