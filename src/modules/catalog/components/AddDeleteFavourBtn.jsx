import { observer } from "mobx-react-lite";
import { useState } from "react";
import { HeartOutlined, HeartFilled } from '@ant-design/icons'
import { favouritesStore } from "../../../common/store/FavouritesStore";

export const AddDeleteFavourBtn = observer(({product, addedToFavour}) => {
    
    const {favourites, addToFavourites, deleteProduct} = favouritesStore;
    const isProductInFavour = () => {
        if (favourites.find((favouritesElement) => favouritesElement.id === product.id)) {
            return true
        } else 
            return false
    } 

    const [added, setAdded] = useState(addedToFavour || isProductInFavour()); 

    const handleClickAddDelete = () => {
        if(!added) {
            addToFavourites(product)
            setAdded(true);
        } else {
            deleteProduct(product.id)
            setAdded(false);
        }
    }


    const handleClickAddDeleteFavour = () => {
        deleteProduct(product.id)
    }

    return (
    <>
        <button 
            onClick={!addedToFavour ? handleClickAddDelete : handleClickAddDeleteFavour} 
            className={`product__favour__btn ${added === true ? 'delete__product' : 'add__product'}`}
        > 
            {!added ? 
                <HeartOutlined /> 
                :
                <HeartFilled />
            }            
        </button>
    </>
    )
})