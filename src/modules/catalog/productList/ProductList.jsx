import { useEffect, useState } from "react"
import { ProductListItem } from './components/ProductListItem'
import { Link, useParams } from "react-router-dom"
import './ProductList.css'
import { Skeleton } from "antd"
import { PageContent, PageTitle } from "../../components"



export const ProductList = () => {

    const {categoryId} = useParams()
    let categoryName;

    const [categories, setCategories] = useState(undefined)
    const loadCategories = async () => {
        fetch(`https://api.jsonbin.io/v3/b/6485bca1b89b1e2299ad4366/`)
            .then(res=>res.json())
            .then(json => {
                setCategories([...json.record.categories])
            }
        )
    }

    useEffect(() => {   
        loadCategories()
    }, [])

    if (categories) {
        categoryName = categories.find((category) =>category.id === categoryId).name
    }

    const [products, setProducts] = useState(undefined)
    const loadProducts = async () => {
        fetch(`https://api.jsonbin.io/v3/b/6485bca1b89b1e2299ad4366`)
            .then(res=>res.json())
            .then(json => {
                let allProducts = json.record.products
                let productList = allProducts.filter((product) => product.category === categoryId)
                setProducts([...productList])
            }
        )
    }

    useEffect(() => {   
        loadProducts()
    }, [])

    return (
        <>
        {!products && !categoryName ? 
            <PageTitle className="skeletons">
                <Skeleton active={true}/>
            </PageTitle> : 
            <PageTitle>
                {categoryName}
            </PageTitle>
        }
        {!products ? 
            <div className="product__list skeletons">
                <Skeleton active={true}/>
                <Skeleton active={true}/>
                <Skeleton active={true}/>
            </div> : 
            <PageContent className="product__list">
            { 
                products.map((product, index) => 
                    <ProductListItem product={product} key={index}/>
                )
            }
            </PageContent>
        }
        
    </>
    )
}