import { observer } from "mobx-react-lite"
import { Link } from "react-router-dom"
import noPhoto from "../../../../img/no__foto.png"
import { AddDeleteCartBtn } from "../../components/AddDeleteCartBtn";
import { AddDeleteFavourBtn } from "../../components/AddDeleteFavourBtn";

export const ProductListItem = observer(({product}) => {

    const {images, name, id, price} = product

    return (
        
        <li className="product__list__item">
            <Link to={`${id}`} className="product__list__item__link">
                <div className="product__list__item__image__wrapper">
                {images === '' || !images ? 
                    <img className='no-photo' src={noPhoto} alt={name}/>
                :
                    <img alt={name} className="product__list__item__image" src={`${images[0]}`} />
                }
                </div>
                <div>
                <span className="product__list__item__name">{name}</span>
                </div>
            </Link>
            <div className="product__list__item__info">
                <span className="product__price">{price} BYN</span>
                <AddDeleteCartBtn product={product}/>
                <AddDeleteFavourBtn product={product}/>
            </div>

        </li>
    )
})