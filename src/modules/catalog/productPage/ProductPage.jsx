import { Skeleton } from 'antd'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { observer } from 'mobx-react-lite';
import { AddDeleteCartBtn } from '../components/AddDeleteCartBtn';
import { Image } from 'antd';
import noPhoto from "../../../img/no__foto.png"
import './ProductPage.css'
import { AddDeleteFavourBtn } from '../components/AddDeleteFavourBtn';
import { PageContent, PageTitle } from '../../components';



export const ProductPage = observer(() => {

    const {productId} = useParams()

    const [product, setProduct] = useState(undefined)
    const loadProduct = async () => {
        fetch(`https://api.jsonbin.io/v3/b/6485bca1b89b1e2299ad4366`)
            .then(res=>res.json())
            .then(json => {
                let allProducts = json.record.products
                let product = allProducts.find((product) => product.id === productId )
                setProduct(product)
            }
        )
    }
    useEffect(() => {   
        loadProduct()
    }, [])

    const clickTest = () => {
    console.log('click')
    }

    return (
    <>
    {!product ? 
            <PageTitle className="skeletons">
                <Skeleton active={true}/>
            </PageTitle> : 
            <PageTitle>
                {product.name}
            </PageTitle>
    }
        {!product ? 
            <div className="product skeletons">
                <Skeleton active={true}/>
            </div> : 
            <PageContent className="product">
                <div className='product__wrapper'>
                    <div className="product__images">
                        {product.images === '' || !product.images ? 
                        <div className='product__image__wrapper product__image__wrapper__main'>
                            <img className='no-photo' src={noPhoto} alt={product.name}/>
                        </div>
                        :
                        <Image.PreviewGroup>
                            <div className='product__image__wrapper product__image__wrapper__main'>
                                <Image className="product__image" src={`${product.images[0]}`} />
                            </div>
                            <div className='product__image__small'>

                                { product.images.map((image, index) => {
                                    if (index !== 0) {
                                        return (
                                            <div className='product__image__wrapper'
                                                key={`img_${index}`}>                                    
                                                <Image className="product__image" src={`${image}`} />
                                            </div>
                                        )                            
                                    }
                                })}
                            </div>
                        </Image.PreviewGroup>
                        }                
                    </div>
                    <div className='product__description'>
                        <h3>Описание товара</h3>
                    {
                        product.description.split('\n').map((str, index) => 
                            <p key={`p_${index}`}>{str}</p>
                        ) 
                    }
                        <div className='product__buy__wrapper'>
                            <div className='product__price'>
                                {product.price} BYN
                            </div>
                            <div className='products__btns'>
                                <AddDeleteCartBtn product={product}/>
                                <AddDeleteFavourBtn product={product}/>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </PageContent>
        }
        
    </>
    )
})