import {Form, Input, Button} from 'antd'
import { useNavigate } from 'react-router-dom'
import { cartStore } from '../../../common/store/CartStore';

import './OrderPage.css'

export const OrderPage = ()=> {
    const navigate = useNavigate();

    const handleOrderFinish = (value) => {
        cartStore.resetCart()
        navigate('../result', {
            state: value
        })
    }

    return (
        <Form 
            className='order__form'
            onFinish={handleOrderFinish}
            style={{ maxWidth: 600}}
        >
         <Form.Item
            className='order__form__item'
            label="ФИО"
            name="userName"
            rules={[
                {
                required: true,
                message: 'Скажите, как можно к вам обращаться',
                }
            ]}
            >
            <Input className='order__form__input' />
        </Form.Item>
        <Form.Item
        className='order__form__item'
        label="Телефон для связи"
        name="phone"
        rules={[
            { 
            required: true,
            message: 'Введите ваш номер, чтобы мы могли с вами связаться' 
            }
        ]}
        >
        <Input className='order__form__input' />
      </Form.Item>     
        <Form.Item>
            <Button className='order__form__btn' type="primary" htmlType="submit">
                Оформить
            </Button>
        </Form.Item>
    </Form>
    )
}