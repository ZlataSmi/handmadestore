import { observer } from "mobx-react-lite";
import { Route, Routes } from "react-router-dom";
import { CartPage } from "./cartPage/CartPage";
import { OrderPage } from "./orderPage/OrderPage";
import { ResultPage } from "./resultPage/ResultPage";


export const Cart = observer(() => {


    return (       
        <Routes>
            <Route index element={<CartPage />} />
            <Route path="/order" element={<OrderPage />} />
            <Route path="/result" element={<ResultPage />} />
        </Routes>
    )
})