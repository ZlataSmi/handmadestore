import { useNavigate } from "react-router-dom"
import { Button, Result } from 'antd';

export const ResultPage = () => {

    const navigate = useNavigate()

    const handleGoCatalogClick = () => {
        navigate ('../../catalog')
    }

    return (
        <Result 
            className="result__content"
            status="success"
            title="Благодарим за покупку"
            subTitle={`Мы свяжемся с вами в ближайшее время для обсуждения деталей заказа, оплаты и доставки. А пока можете присмотреть себе что-нибудь ещё`}
            extra={[
                <Button className='order__form__btn' onClick={handleGoCatalogClick} key="buy">Каталог подарков</Button>,
            ]}
        />
    )
}