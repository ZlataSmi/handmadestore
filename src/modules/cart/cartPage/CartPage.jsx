import { observer } from "mobx-react-lite";
import { Link, useNavigate } from "react-router-dom";
import { AddDeleteCartBtn } from "../../catalog/components/AddDeleteCartBtn";
import { cartStore } from "../../../common/store/CartStore";
import { Image } from 'antd';
import { PageContent, PageTitle } from "../../components";
import noPhoto from "../../../img/no__foto.png"
import './CartPage.css'

export const CartPage = observer(() => {
    const navigate = useNavigate();
    const {cart, resetCart} = cartStore;


    const handleOrderClick = () => {
        navigate('./order')
    }

    const countTotalPrice = () => {
        let count = 0;
        cart.map(product => {
            count += +product.price
        })
        return count
    }

    return (
    <>
        <PageTitle>
            Корзина товаров
        </PageTitle>
        <PageContent className="cart">
        {
        cart.length === 0 ? 
        <h3>Корзина пуста</h3>
        :
        <>
            <div className="cart__table">
                {cart.map((product, index) => 
                    <div className="cart__product" key={index}>
                        <div className="product__images">
                        {product.images === '' || !product.images ? 
                        <div className='product__image__wrapper product__image__wrapper__main'>
                            <img className='no-photo' src={noPhoto} alt={product.name}/>
                        </div>
                        :
                        <Image.PreviewGroup>
                            <div className='product__image__wrapper product__image__wrapper__main'>
                                <Image className="product__image" src={`${product.images[0]}`} />
                            </div>
                        </Image.PreviewGroup>
                        }                
                        </div>
                        <Link className="cart__product__name" to={`../../catalog/${product.category}/${product.id}`}>{product.name}</Link>
                        <h3>{product.price} BYN</h3>
                        <AddDeleteCartBtn product={product} addedToCart={true} />
                        <hr/>
                    </div>
                    
                )}
                <hr/>
            </div>
            <div className="cart__total">            
                <div className="total__price">Итого {countTotalPrice()} BYN</div>
                <button className="cart__btn cart__reset__btn" onClick={resetCart}>Очистить корзину</button>
                <button className="cart__btn cart__order__btn"  onClick={handleOrderClick} >Оформить заказ</button>
            </div>
        </>

    }
    </PageContent>
    </>
    )
})