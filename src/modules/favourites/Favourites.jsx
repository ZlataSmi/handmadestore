import { Image } from "antd";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import { favouritesStore } from "../../common/store/FavouritesStore";
import { AddDeleteFavourBtn } from "../catalog/components/AddDeleteFavourBtn";
import noPhoto from "../../img/no__foto.png"
import { PageContent, PageTitle } from "../components";
import { AddDeleteCartBtn } from "../catalog/components/AddDeleteCartBtn";

export const Favourites = observer(() => {

    const {favourites, resetFavourites} = favouritesStore;

    return (
    <>
        <PageTitle>
            Избранное
        </PageTitle>
        <PageContent className="favourites">
        {
        favourites.length === 0 ? 
        <h3>В Избранном ничего нет</h3>
        :
        <>
        <div className="cart__table">
                {favourites.map((product, index) => 
                    <div className="cart__product" key={index}>
                        <div className="product__images">
                        {product.images === '' || !product.images ? 
                        <div className='product__image__wrapper product__image__wrapper__main'>
                            <img className='no-photo' src={noPhoto} alt={product.name}/>
                        </div>
                        :
                        <Image.PreviewGroup>
                            <div className='product__image__wrapper product__image__wrapper__main'>
                                <Image className="product__image" src={`${product.images[0]}`} />
                            </div>
                        </Image.PreviewGroup>
                        }                
                        </div>
                        <Link className="cart__product__name" to={`../../catalog/${product.category}/${product.id}`}>{product.name}</Link>
                        <h3>{product.price} BYN</h3>
                        <div className="products__btns">
                        <AddDeleteCartBtn product={product}/>
                        <AddDeleteFavourBtn product={product} addedToFavour={true} />
                        </div>
                        <hr/>
                    </div>
                    
                )}
                <hr/>
            </div>
            <div className="cart__total">            
                <button className="cart__btn cart__reset__btn" onClick={resetFavourites}>Очистить Избранное</button>
            </div>

        </>

        }
        </PageContent>
    </>
    )
})