import { makeAutoObservable} from "mobx"

class CartStore {

    cart = JSON.parse(localStorage.getItem('cart')) || [];

    constructor() {
        makeAutoObservable(this)
    }

    get count () {
        let count = 0
        if (this.cart.length === 0 ) {
            return 0
        }

        this.cart.forEach((product) => {
            count ++;
        })

        return count
    }

    addToCart = (product) => {
        if(!this.cart.find((element) => element.id === product.id))
        {
            this.cart.push(product)
            this.updateLocalStorage()
        }

    }

    deleteProduct = (productId) => {
        this.cart = this.cart.filter(({id}) => id !== productId)
        this.updateLocalStorage()        
    }

    updateLocalStorage = () => {
        localStorage.setItem('cart', JSON.stringify(this.cart))
    }

    resetCart = () => {
        this.cart = [];
        localStorage.removeItem('cart')
    }

    
}

export const cartStore = new CartStore()