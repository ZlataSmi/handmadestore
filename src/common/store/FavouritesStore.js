import { makeAutoObservable} from "mobx"

class FavouritesStore {

    favourites = JSON.parse(localStorage.getItem('favourites')) || [];

    constructor() {
        makeAutoObservable(this)
    }

    get countFavourites () {
        let count = 0
        if (this.favourites.length === 0 ) {
            return 0
        }

        this.favourites.forEach((product) => {
            count ++;
        })

        return count
    }

    addToFavourites = (product) => {
        this.favourites.push(product)
        this.updateLocalStorage()
    }

    deleteProduct = (productId) => {
        this.favourites = this.favourites.filter(({id}) => id !== productId)
        this.updateLocalStorage()        
    }

    updateLocalStorage = () => {
        localStorage.setItem('favourites', JSON.stringify(this.favourites))
    }

    resetFavourites = () => {
        this.favourites = [];
        localStorage.removeItem('favourites')
    }

    
}

export const favouritesStore = new FavouritesStore()