import { NavLink } from "react-router-dom"
import { Form, Input, Button, Modal } from "antd"
import Icon, { InstagramOutlined, FacebookOutlined, YoutubeOutlined } from '@ant-design/icons'
import logo from '../../../img/logo.jpg';
import './Footer.css'
import { useRef } from "react";

export const Footer = () => {


    const PinterestSvg = () => (
        <svg width="1em" height="1em" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
        <path d="M204 6.5C101.4 6.5 0 74.9 0 185.6 0 256 39.6 296 63.6 296c9.9 0 15.6-27.6 15.6-35.4 0-9.3-23.7-29.1-23.7-67.8 0-80.4 61.2-137.4 140.4-137.4 68.1 0 118.5 38.7 118.5 109.8 0 53.1-21.3 152.7-90.3 152.7-24.9 0-46.2-18-46.2-43.8 0-37.8 26.4-74.4 26.4-113.4 0-66.2-93.9-54.2-93.9 25.8 0 16.8 2.1 35.4 9.6 50.7-13.8 59.4-42 147.9-42 209.1 0 18.9 2.7 37.5 4.5 56.4 3.4 3.8 1.7 3.4 6.9 1.5 50.4-69 48.6-82.5 71.4-172.8 12.3 23.4 44.1 36 69.3 36 106.2 0 153.9-103.5 153.9-196.8C384 71.3 298.2 6.5 204 6.5z"/></svg>
    );

    const PinterestIcon = (props) => <Icon component={PinterestSvg} {...props} />;

    const success = () => {
        onReset()
        Modal.success({
          content: (
            <div>
                <p>Готово</p>
                <p>Обещаем, мы будем присылать только самые интересные новсти и никакой рекламы</p>
            </div>
        ),
        });
    };

    const formRef = useRef(null);

    const onReset = () => {
        formRef.current?.resetFields();
      };

    return (
        <footer className="footer">
            <div className="content">
                <div className='footer__wrapper'>
                    <div className="footer__logo">
                        <NavLink to="/">
                            <img src={logo} alt="logo"></img>
                        </NavLink>
                    </div>
                    <p className="footer__text">Если у вас есть уникальная идея, которую вы бы хотели, чтобы мы воплотили для вас, смелее пишите или звоните. Мир фантазии безграничен!</p>
                    <a className="footer__links" href="tel:+375291234567">+375 29 123 45 67</a>
                    <a className="footer__links" href="email:artistic.craft@gmail.com">artistic.craft@gmail.com</a>
                    <p className="footer__text">Подписывайтесь на новости, чтобы первыми видеть новые работы</p>
                    <Form className="footer__form" 
                        onFinish={success}
                        ref={formRef}>
                        <Form.Item
                            
                            name="email"
                            rules={[
                            {
                                type: "email",
                                message: 'Неверный email',
                            },
                            {
                                required: true,
                                message: 'Поле обязательное',
                            }
                            ]}
                        >
                        <Input placeholder="email" className="footer__form__input"/>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className='footer__form__btn'>
                                Подписаться
                            </Button>
                        </Form.Item>
                    </Form>
                    <div className="footer__social-links">
                        <a href="#"><InstagramOutlined /></a>
                        <a href="#"><FacebookOutlined /></a>
                        <a href="#"><YoutubeOutlined /></a>
                        <a href="#"><PinterestIcon/></a>                       
                    </div>
                </div>
            </div>
        </footer>
    )
}