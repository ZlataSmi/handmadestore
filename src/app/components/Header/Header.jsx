import { NavLink, useLocation } from "react-router-dom"
import { ShoppingCartOutlined, HeartOutlined, MenuOutlined } from '@ant-design/icons'
import { useEffect, useState } from 'react';
import './Header.css'
import logo from '../../../img/logo.jpg';
import { cartStore } from "../../../common/store/CartStore";
import { observer } from "mobx-react-lite";
import { favouritesStore } from "../../../common/store/FavouritesStore";

export const Header = observer(() => {

    const [toggle, setToggle] = useState(false);
    const location = useLocation();
    const {count} = cartStore
    const {countFavourites} = favouritesStore

    useEffect(() => {
      if (toggle === true) {
        setToggle(false)
      }
    }, [location]);
    
    return (
        <header className="header">
            <div className="content">
                <div className="header__wrapper"> 
                    <div className="header__logo">
                        <NavLink to="/">
                            <img src={logo} alt="logo"></img>
                        </NavLink>
                    </div>
                    <div 
                        onClick={()=>{setToggle(!toggle)}} 
                        className={`nav__toggle__btn ${toggle ? 'nav__toggle__btn__toggled' : ''}`}>
                        <MenuOutlined />
                    </div>
                    <nav className={`header__nav ${toggle ? 'header__nav__toggled' : ''}`}>
                        <li className="nav__item">
                            <NavLink className='nav__link' to="/catalog">Каталог</NavLink>
                        </li>
                        <li className="nav__item">
                            <NavLink className='nav__link' to="/delivery">Доставка и оплата</NavLink>
                        </li>
                        <li className="nav__item">
                            <NavLink className='nav__link' to="/about">О нас</NavLink>
                        </li>
                    </nav>
                    <div className="header__cart-favour">
                        <li className="header__cart-favour__item">
                            <NavLink to="/cart"><ShoppingCartOutlined/><sup className="header__cart-favour__count">{count}</sup></NavLink>
                        </li>
                        <li className="header__cart-favour__item">
                            <NavLink to="/favourites"><HeartOutlined /><sup className="header__cart-favour__count">{countFavourites}</sup></NavLink>
                        </li>
                    </div>
                </div>
            </div>
        </header>
    )
})