import { Routes, Route } from 'react-router-dom'
import { Home } from '../../modules/home' 
import { Cart } from '../../modules/cart'
import { Catalog } from '../../modules/catalog'
import { AboutUs } from '../../modules/about'
import { Delivery } from '../../modules/delivery'
import { Favourites } from '../../modules/favourites'


export const AppRoutes = () => {
    return (
        <Routes>
            <Route index element={<Home/>}></Route>
            <Route path='/cart/*' element={<Cart/>}></Route>
            <Route path='/favourites/*' element={<Favourites/>}></Route>
            <Route path='/catalog/*' element={<Catalog/>}></Route>
            <Route path='/delivery/' element={<Delivery/>}></Route>
            <Route path='/about/' element={<AboutUs/>}></Route>
        </Routes>
    )
}