import { AppRoutes } from "../AppRoutes"
import './Main.css'

export const Main = () => {
    return (
        <main className="main">
            <div className="content">
                <AppRoutes/>
            </div>
        </main>
    )
}



